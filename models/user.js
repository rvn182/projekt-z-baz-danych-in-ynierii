/**
 * Created by jacek on 2015-05-03.
 */

var Model = require('./model').Model;
var ErrorCode = require('./model').ErrorCode;
var bcrypt = require('bcrypt');

module.exports = Object.create(Model, {
    //nazwa tabeli
    table: {value: 'uzytkownik'},
    verifyCredentials: {
        value: function (email, password, callback) {
            this.selectRowByField({email: email}, function (err, user) {
                if (err === ErrorCode.NOT_FOUND)
                    callback(err, null);
                else {
                    bcrypt.compare(
                        password, user.haslo, function (err, res) {
                            if (res)
                                callback(null, user);
                            else
                                callback(ErrorCode.INVALID_PASSWORD, null);
                        });
                }
            });
        }
    },
    
    create: {
        value: function (user, callback) {
            var that = this;
            this.selectMaxInColumn('id_uzytkownika', function (err, max) {
                if (err === ErrorCode.NOT_FOUND)
                    user.id_uzytkownika = 0;
                else
                    user.id_uzytkownika = max + 1;
                bcrypt.genSalt(
                    10, function (err, salt) {
                        bcrypt.hash(
                            user.haslo, salt, function (err, hash) {
                                user.haslo = hash;
                                that.insert(user, function (result) {
                                    callback(user.id_uzytkownika);
                                });
                            });
                    });
            });
        }
    },
    
    SelectAllParticipants: 
    {
        value:function(callback){
            this.connection.query('select imie, nazwisko, adres, firma, email from uzytkownik where uczestnik=1', 
            function(err, rows, fields) {
            console.log(rows);
            callback(rows);
            });
        }
    },
    
        SelectAllAdmins: 
    {
        value:function(callback){
            this.connection.query('select imie, nazwisko, adres, firma, email from uzytkownik where adm=1', 
            function(err, rows, fields) {
            console.log(rows);
            callback(rows);
            });
        }
    },
    
        SelectAllReviewers: 
    {
        value:function(callback){
            this.connection.query('select imie, nazwisko, adres, firma, email from uzytkownik where recenzent=1', 
            function(err, rows, fields) {
            console.log(rows);
            callback(rows);
            });
        }
    },
        
        SelectAllLoad: 
    {
        value:function(callback){
            this.connection.query('select u1.imie as imier, u1.nazwisko as nazwiskor, u2.imie as imieu, u2.nazwisko as nazwiskou, rf.nazwa as nazwar from uzytkownik u1, uzytkownik u2, recenzja rc, referat rf, uczestnik ucz where (rc.uzytkownik_id_uzytkownika=u1.id_uzytkownika) and (rc.referat_id_referat = rf.id_referat and rf.uczestnik_id_uczestnik = ucz.id_uczestnik and ucz.uzytkownik_id_uzytkownika=u2.id_uzytkownika)', 
            function(err, rows, fields) {
            console.log(rows);
            callback(rows);
            });
        }
    }, 
    
    selectUser: {
        value: function (id, callback) {
        this.connection.query('SELECT * FROM ?? WHERE id_uzytkownika = ?',
            [this.table, id], function (err, rows, fields) {
                if (err) throw err;
                else if (rows.length === 0)
                    callback(ErrorCode.NOT_FOUND, null);
                else
                    callback(null, rows[0]);
            }
            );
        }
    },
    
    updateUser: {
        value: function (id, user, callback) {
        this.connection.query('UPDATE ?? SET ? WHERE id_uzytkownika = ?',
            [this.table , user, id], function (err, result) {
                if (err) throw err;
                else
                callback();
            }
            );
        }
    },
    
    deleteUser: {
        value: function (id,callback) {
        this.connection.query('DELETE FROM ?? WHERE id_uzytkownika = ?',
            [this.table ,id], function (err, result) {
                if (err) throw err;
                else
                callback();
            }
            );
        }
    },
    
});