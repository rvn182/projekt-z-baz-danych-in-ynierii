var Model=require('./model').Model;
var ErrorCode = require('./model').ErrorCode;

module.exports=Object.create(Model,{
    table: {value: 'konferencja'},
    table3: {value: 'przypisanie'},
    table4: {value: 'oplaty'},
    table5: {value: 'uzytkownik'},
    GetUsers:{
    value : function(id, callback){
        var key=id;
        this.connection.query('select u.id_uzytkownika as id_uzytkownika, u.imie as imie, u.nazwisko as nazwisko, k.nazwa as nazwa, p.wplata as wplata, p.id_przypisanie as id_przypisanie from uzytkownik u, konferencja k, przypisanie p where u.id_uzytkownika=p.uzytkownik_id_uzytkownika and k.id_konferencja=p.konferencja_id_konferencja and k.id_konferencja=?',
        [id.id_konferencja],function (err, rows, fields)
        {
            console.log(rows);
            callback(rows);
        });
    }
    },
    ShowConferences2:{
    value : function(callback){
        
        this.connection.query('select k.nazwa as nazwa , sum(o.kwota) as kwota, k.id_konferencja as id_konferencja from konferencja k left join oplaty o on k.id_konferencja=o.konferencja_id_konferencja group by k.id_konferencja',
        function (err, rows, fields)
        {
            console.log(rows);
            callback(rows);
        });
    }
    },
    ShowSumCost:{
    value : function(id,callback){
        
        this.connection.query('select sum(o.kwota) as kwota from konferencja k left join oplaty o on k.id_konferencja=o.konferencja_id_konferencja group by k.id_konferencja having k.id_konferencja=?',
        [id.id_konferencja], function (err, rows, fields)
        {
            console.log(rows);
            callback(rows);
        });
    }
    },
    selectPayment:{
    value : function(id, callback){
        //id.id_przyp//
        var key=id;
        this.connection.query('select p.id_przypisanie as id_przypisanie from przypisanie p where p.id_przypisanie=?',
        [id],function (err, rows, fields)
        {
            console.log(rows);
            callback(rows);
        });
    }
    },
    showProducts:{
    value : function(id, callback){
        //id.id_przyp//
        var key=id;
        this.connection.query('select k.nazwa as nazwak, o.id_oplaty, o.kwota as kwota, o.nazwa as nazwa, k.id_konferencja as id_konferencja, o.konferencja_id_konferencja as konferencja_id_konferencja from konferencja k, oplaty o where o.konferencja_id_konferencja=k.id_konferencja and k.id_konferencja=?',
        [id.id_konferencja],function (err, rows, fields)
        {
            console.log(rows);
            callback(rows);
        });
    }
    },
    createCost:{
        //table: {value: 'oplaty'},
        value: function (charges, callback) {
            var that = this;
            this.selectMaxInColumnO('id_oplaty', function (err, max) {
                if (err === ErrorCode.NOT_FOUND)
                    charges.id_oplaty = 0;
                else
                    charges.id_oplaty = max + 1;
                that.insertO(charges, function (result) {
                    callback(charges.id_oplaty);
                                });
            });
        }
    },
    showConferenceName:{
    value : function(id, callback){
        //id.id_przyp//
        var key=id;
        this.connection.query('select nazwa from konferencja where id_konferencja=?',
        [id.id_konferencja],function (err, rows, fields)
        {
            console.log(rows);
            callback(rows);
        });
    }
    },
    setPayment:{
    value : function(id, callback){
        // id: 2;
        // console.log(id);
        //var key = Object.keys(id).pop();
        this.connection.query('update przypisanie set wplata=true where id_przypisanie=?',[id],
        function (err, result)
        {
            callback();
        });
    }
    },
    selectRowByFieldP:{ 
        value :function (field, callback) {
        var key = Object.keys(field).pop();
        this.connection.query('SELECT * FROM ?? WHERE ?? = ?',
            [this.table3, key, field[key]], function (err, rows, fields) {
                if (err) throw err;
                else if (rows.length === 0)
                    callback(ErrorCode.NOT_FOUND, null);
                else
                    callback(null, rows.pop());
            });
        }
        },
    updateRowByFieldP:{
        
        value: function (field, value, callback) {
        
        var key = Object.keys(field).pop();
        this.connection.query('UPDATE ?? SET ? WHERE ?? = ?',
            [this.table3, value, key, field[key]], function (err, result) {
                if (err)
                    throw err;
                else
                    callback();
            });
        }
        },
    selectRowByFieldO:{ 
        value :function (field, callback) {
        var key = Object.keys(field).pop();
        this.connection.query('SELECT * FROM ?? WHERE ?? = ?',
            [this.table4, key, field[key]], function (err, rows, fields) {
                if (err) throw err;
                else if (rows.length === 0)
                    callback(ErrorCode.NOT_FOUND, null);
                else
                    callback(null, rows.pop());
            });
        }
        },
    deleteCost:{
    value : function(id, callback){
        //id.id_przyp//
        var key=id;
        this.connection.query('delete from oplaty where id_oplaty=?',
        [id.id_oplaty],function (err, rows, fields)
        {
            callback();
            console.log(rows);
        });
    }
    },
    selectRowByFieldU:{ 
        value :function (field, callback) {
        var key = Object.keys(field).pop();
        this.connection.query('SELECT * FROM ?? WHERE ?? = ?',
            [this.table5, key, field[key]], function (err, rows, fields) {
                if (err) throw err;
                else if (rows.length === 0)
                    callback(ErrorCode.NOT_FOUND, null);
                else
                    callback(null, rows.pop());
            });
        }
        },
    selectMaxInColumnO:{ 
        value: function (column, callback) {
        this.connection.query(
            'SELECT MAX(??) FROM ??',
            [column, this.table4], function (err, rows, fields) {
                var query = 'MAX(`' + column + '`)';
                if (err)
                    throw err;
                else if (rows.length === 0 || rows[0][query] === null)
                    callback(ErrorCode.NOT_FOUND, null);
                else
                    callback(null, rows.pop()[query]);
            });
    }
    },
    insertO:{ 
        value: function (row, callback) {
        this.connection.query('INSERT INTO ?? SET ?',
            [this.table4, row], function (err, result) {
                if (err)
                    throw err;
                else
                    callback(result);
            });
    }
    },
    selectSumCost:{ 
        value :function (field, callback) {
        var key = Object.keys(field).pop();
        this.connection.query('SELECT sum(o.kwota) as kwota FROM ?? LEFT JOIN oplaty o on ??.??=o.konferencja_id_konferencja where ??.?? = ?',
            [this.table, this.table, key, this.table, key, field[key]], function (err, rows, fields) {
                if (err) throw err;
                else if (rows.length === 0)
                    callback(ErrorCode.NOT_FOUND, null);
                else
                    callback(null, rows.pop());
            });
        }
    },
});