/**
 * Created by jacek on 27.05.15.
 */
var Model = require('./model').Model;
var ErrorCode = require('./model').ErrorCode;


module.exports = Object.create(Model, {
    table: {value: 'hotel'},
    
     SelectAllAccommodation: 
    {
        value:function(callback){
            this.connection.query('select u.imie as imie, u.nazwisko as nazwisko, u.oplaty as oplaty, h.nazwa as nazwah, h.adres as adres from uzytkownik u join uczestnik ucz on ucz.uzytkownik_id_uzytkownika=u.id_uzytkownika join hotel h on ucz.hotel_id_hotel=h.id_hotel where (u.uczestnik=1 and recenzent=0) or (u.uczestnik=0 and recenzent=1)', 
            function(err, rows, fields) {
            console.log(rows);
            callback(rows);
            });
        }
    },
    create:{
        value: function (hotel, callback) {
            var that = this;
            this.selectMaxInColumn('id_hotel', function (err, max) {
                if (err === ErrorCode.NOT_FOUND)
                    hotel.id_hotel = 0;
                else
                    hotel.id_hotel = max + 1;
                that.insert(hotel, function (result) {
                    callback(hotel.id_hotel);
                });
            });
        }
    },
    showHotels:{
    value : function(callback){
        this.connection.query('select * from hotel',
        function (err, rows, fields)
        {
            console.log(rows);
            callback(rows);
        });
    }
    }
});
