/**
 * Created by jacek on 14.05.15.
 */

var LocalStrategy = require('passport-local').Strategy;
var user = require('../models/user');
var ErrorCode = require('../models/model').ErrorCode;

module.exports = function (passport) {
    passport.serializeUser(function (user, done) {
        done(null, user.id_uzytkownika);
    });

    passport.deserializeUser(function (id, done) {
        user.selectRowByField({id_uzytkownika: id}, function (err, user) {
            done(null, user);
        });
    });

    passport.use(
        new LocalStrategy({
                usernameField: 'email',
                passwordField: 'haslo'
            },
            function (email, password, done) {
                user.verifyCredentials(
                    email, password, function (err, user) {
                        switch(err) {
                            case null:
                                return done(null, user);
                            case ErrorCode.NOT_FOUND:
                                return done(null, false, {message:"Nie znaleziono użytkownika o danej nazwie"});
                            case ErrorCode.INVALID_PASSWORD:
                                return done(null, false, {message:"Podano złe hasło"});
                            default:
                                return done(err, false);
                        }
                    });
            }));
};