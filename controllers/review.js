/**
 * Created by kamil on 2015-05-20.
 */
var express = require('express');
var review = require('../models/review');
var comment = require('../models/comment');
var dateUtil = require('../common/dateutil');
var auth = require('../config/auth');
var async = require('async');
var user= require('../models/user');
var checkUtil = require('../common/checkutil');

module.exports = function (passport) {
    var router = express.Router();

    
    router.get('/review', function(req, res, next) {
        res.render('review', { title: "Zarządzaj recenzjami" });
    });
    
    router.get('/review/:id', function(req, res) {
        review.selectRowByField({id_recenzja: req.params.id}, function(err, result) {
            if(err)
                throw err;
                
            comment.getForReview(req.params.id, function(comments) {
                async.concat(comments.map(function(c) { return c.uzytkownik_id_uzytkownika; }), function(id, cb) {
                    user.selectRowByField({id_uzytkownika: id}, function(err, user) {
                        if(err)
                            cb(err, null);
                        else    
                            cb(null, user.imie + ' ' + user.nazwisko);
                    });
                }, function(err, users) {
                    if(err)
                        throw err;
                    var formatted = comments.map(function(com, i) {
                        com.autor = users[i];
                        com.data = dateUtil.format(com.data);
                        return com;
                    });
                    res.render('reviewer/review', {review: result, comments: formatted});
                });
            });
        });
    });
    
    router.get('/send_review/:id', auth.isReviewer, function(req, res) {
        res.render('reviewer/send_review', {id: req.params.id, message: req.flash('error')[0]});
    });
    
    router.post('/review/:id', auth.isReviewer, function(req, res) {
        if(!checkUtil.checkForField(req, 'tresc')) {
            req.flash('error', 'Nie podano treści!')
            res.redirect('/send_review/' + req.params.id);
        }
        else if(!checkUtil.checkForField(req, 'nazwa')) {
            req.flash('error', 'Nie podano nazwy!')
            res.redirect('/send_review/' + req.params.id);
        }
        else {
            var obj = {
                nazwa: req.body.nazwa,
                tresc: req.body.tresc,
                zatwierdzona: req.body.zatwierdz === 'on',
                wyrozniony: req.body.wyroznij === 'on',
                ocena_zgodnosci: req.body.zgod,
                ocena_oryginalnosci: req.body.oryg,
                ocena_jakosci: req.body.jakosc,
                ocena_poprawnosci: req.body.popr,
                p_recenzenta_id_przypisania: req.params.id
            }
            review.add(obj, function(id) {
                res.redirect('/review/' + id);
            });
        }
    })
    
    router.post('/comment/:id', auth.isUser, function(req, res) {
         comment.add({
            recenzja_id_recenzja: req.params.id,
            tresc: req.body.content,
            uzytkownik_id_uzytkownika: req.user.id_uzytkownika     
         }, function() {
             res.redirect('/review/' + req.params.id);
         });
    });
    
    router.get('/review_add', function(req, res, next) {
        res.render('review_add');
    });
    
    router.get('/review_edit', function(req, res, next) {
        res.render('review_edit');
    });
    
    router.get('/review_show', function(req, res, next) {
        res.render('review_show');
    });
    
    router.get('/review_comment', function(req, res, next) {
        res.render('review_comment');
    });
    
    router.get('/review_browse', function(req, res) {
        review.selectAll(function(rows) {
            res.render('review_browse', {review: rows});
        });
    })

    return router;
}