/**
 * Created by jacek on 2015-05-12.
 */

var express = require('express');
var paper = require('../models/paper');
var session = require('../models/session');
var user = require('../models/user');
var review = require('../models/review');
var conference = require('../models/conference');
var participant = require('../models/participant');
var reviewer = require('../models/reviewer');
var async = require('async');
var checkUtil = require('../common/checkutil');

module.exports = function (passport) {
    var router = express.Router();

    router.get('/show_paper/:id', function (req, res) {
        paper.selectRowByField({id_referat: req.params.id}, function (err, paper) {
            res.render('paper/show_paper', {paper: paper});
        });
    });

    router.get('/send_paper', function(req,res){
            conference.getAvailableParticipantConferences(req.user.id_uzytkownika, function(rows) {
                res.render('send_paper', {conferences: rows});
            });
    });

   router.get('/paper/:id', function (req, res) {
        paper.selectRowByField({id_referat: req.params.id}, function (err, paper) {
             res.render('paper', {paper: paper});
        });
    });
    
    router.get('/list_user_paper', function (req, res, next) {
        paper.getCreatedPapers({id_uzytkownika: req.user.id_uzytkownika}, function(paper){
            console.log(paper);
            res.render('list_user_paper', {paper: paper});
        });
    });

    router.get('/list_paper', function (req, res) {
        paper.selectAllPapers(function(all){
            res.render('paper/list_paper', {wynik: all, message: req.flash('info')[0] });
        });
    });

    router.get('/assign_paper/:id', function (req, res) {
            paper.selectAllReviewers(function(reviewers){
                paper.selectRowByField({id_referat: req.params.id}, function (err, paper) {
                    res.render('paper/assign_paper', {referat:paper, recenzenci:reviewers,message: req.flash('info')[0] });
        });
        });
    });
     router.post('/assign_paper/:id', function (req, res) {
        paper.selectRowByField({id_referat: req.params.id}, function (err, referat){
            paper.countAllAssignedReviewers(req.params.id,function(err,number){
                reviewer.selectMaxInColumn('id_przypisania', function (err, id_przypisanie){
                    paper.selectAllAssignedReviewers(referat.id_referat, function (recenzenci){
                    
            if(req.body.Recenzent1===req.body.Recenzent2){
                req.flash('info','Dwóch takich samych recenzentów');
                res.redirect('/assign_paper/'+ req.params.id);   
            }
            else{
            if(number ===0){
                var obj1 = {
                        referat_id_referat:referat.id_referat,
                        uzytkownik_id_uzytkownika:req.body.Recenzent1,
                    };
                var obj2 = {
                        referat_id_referat:referat.id_referat,
                        uzytkownik_id_uzytkownika:req.body.Recenzent2,
                };
                reviewer.add(obj1, function (id_przypisanie) {
                     reviewer.add(obj2, function (id_przypisanie) {
                        res.redirect('/list_paper');
                });
                });
            }else{
                recenzent1=recenzenci[0].id_przypisania;
                recenzent2=recenzenci[1].id_przypisania;
                var obj3 = {
                        referat_id_referat:referat.id_referat,
                    };
                var obj4 = {
                        referat_id_referat:referat.id_referat,
                };
                reviewer.updateReviewer(recenzent1,obj3, function () {
                     reviewer.updateReviewer(recenzent2,obj4, function () {
                        res.redirect('/list_paper');
                    });
                    });
            }
            }
        });
        });   
    }); 
    });
    });
    
    router.get('/verify_paper/:id', function (req, res) {
        paper.selectRowByField({id_referat: req.params.id}, function (err, paper) {
            res.render('paper/verify_paper', {paper: paper});
        });
    });
    router.post('/verify_paper/:id', function (req, res) {
        var obj = {
            poprawiony: req.body.poprawiony==='on',
            zatwierdzony: req.body.zatwierdzony==='on'
        };
        paper.updateRowByField({id_referat: req.params.id}, obj, function () {
            res.redirect('../list_paper');
        });
    });
    

  router.get('/add_paper', function (req, res) {
        
        user.selectAll(function (all_user){
        conference.selectAll(function(all_conference){
             async.concat(all_user, function(user, cb) {
                    conference.getAllForUser(user.id_uzytkownika, function(konferencje) {
                        cb(null, {user: user, confs: konferencje.map(function(konf) {return konf.konferencja_id_konferencja })});
                    });
                }, function(err, data) {
                    if(err)
                        throw err;
                    res.render('paper/add_paper', {users: data,konferencje:all_conference,message: req.flash('info')[0] });
                });
                });
        });
    });
    
    

    router.post('/paper', function (req, res) {
        if (!checkUtil.checkForField(req, 'nazwa'))
            res.render('send_paper', {error: "Nie podano nazwy referatu"});
        else {
                var obj = {
                    nazwa: req.body.nazwa,
                    przypisanie_id_przypisanie: req.body.konferencje,
                    tresc: req.body.zawartosc,
                };
                paper.add(obj, function (id) {
                    res.redirect('/paper/' + id);
                });
        }
    });
    
    router.post('/add_paper', function(req,res){
        if (!checkUtil.checkForField(req, 'nazwa')){
            req.flash('info', 'Nie podano tytułu referatu.');
            res.redirect('add_paper');
        }
        else if (!checkUtil.checkForField(req, 'tresc')){
            req.flash('info', 'Nie podano treści referatu.');
            res.redirect('add_paper');
        }
        else{
            
            paper.selectAssignement(req.body.autor, req.body.konferencja, function (przypisanie){
            var obj = {
                nazwa: req.body.nazwa,
                tresc: req.body.tresc,
                przypisanie_id_przypisanie:przypisanie.id_przypisanie,
            };
            paper.add(obj, function (id) {
                req.flash('info', 'Referat dodany do bazy.');
                res.redirect('/list_paper');
            });
        });
        }
    });
    router.get('/edit_paper/:id', function (req, res) {
        
        var id=req.params.id;
        paper.selectRowByField({id_referat: req.params.id}, function (err, paper) {
            
            res.render('paper/edit_paper', {edit_paper: paper});
        });
    });
    router.post('/edit_paper/:id', function (req, res) {
        
        var obj = {
            nazwa: req.body.nazwa,
            tresc: req.body.tresc,
        };
        var id=req.params.id;
        paper.updatePaper(id, obj, function () {
            res.redirect('/list_paper');
        });
    });
     router.get('/delete_paper/:id', function (req, res) {
        
        paper.deletePaper(req.params.id, function () {
            res.redirect('/list_paper');
        });
    });
    return router;
    
};
