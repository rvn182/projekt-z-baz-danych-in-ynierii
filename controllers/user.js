/**
 * Created by jacek on 14.05.15.
 */

var express = require('express');
var user = require('../models/user');
var auth = require('../config/auth');
var checkUtil = require('../common/checkutil');

module.exports = function (passport) {
    var router = express.Router();

    router.get('/login', function (req, res, next) {
        var errors = req.flash('error');
        if (errors.length > 0)
            res.render('login', {error: errors.pop()});
        else
            res.render('login');
    });

    router.post('/login', passport.authenticate('local', {
        successRedirect: '/',
        failureRedirect: '/login',
        failureFlash: true
    }));

    router.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });

    router.get('/register', function (req, res, next) {
        res.render('register');
    });

    router.post('/register', function (req, res) {
        if (!checkUtil.checkForField(req, 'email'))
            res.render('register', {error: "Nie podano e-maila"});
        else if (!checkUtil.checkForField(req, 'haslo'))
            res.render('register', {error: "Nie podano hasła"});
        else if (!checkUtil.checkForField(req, 'imie'))
            res.render('register', {error: "Nie podano imienia"});
        else if (!checkUtil.checkForField(req, 'nazwisko'))
            res.render('register', {error: "Nie podano nazwiska"});
        else {
            var obj = {
                email: req.body.email,
                imie: req.body.imie,
                nazwisko: req.body.nazwisko,
                adres: req.body.adres,
                firma: req.body.firma,
                nr_tel: req.body.nrtel,
                haslo: req.body.haslo,

            };
            user.create(obj, function () {
                res.render('login', {message: "Rejestracja przebiegła pomyślnie! Teraz można sie zalogować."});
            });
        }
    });

    router.get('/add_user', auth.isAdmin, function (req, res, next) {
        res.render('users/add_user');
    });

    router.post('/add_user', auth.isAdmin, function (req, res, next) {
        
         var check = function (field) {
            return req.body.hasOwnProperty(field) && req.body[field].length > 0;
        };

        if (!checkUtil.checkForField(req, 'email'))
            res.render('users/add_user', {message: "Nie podano e-maila"});
        else if (!checkUtil.checkForField(req, 'password'))
            res.render('users/add_user', {message: "Nie podano hasła"});
        else if (!checkUtil.checkForField(req, 'imie'))
            res.render('users/add_user', {message: "Nie podano imienia"});
        else if (!checkUtil.checkForField(req, 'nazwisko'))
            res.render('users/add_user', {message: "Nie podano nazwiska"});
        else {
        
        var obj = {
            email: req.body.email,
            imie: req.body.imie,
            nazwisko: req.body.nazwisko,
            adres: req.body.adres,
            firma: req.body.firma,
            nr_tel: req.body.phone,
            haslo: req.body.password,
            aktywny: req.body.optionsStatus === 'aktywny',
            uczestnik: 0,
            adm: req.body.admin === 'on',
            oplaty: req.body.optionsOplata === 'dokonano',
            komentarz_admin: req.body.komentarz,
            recenzent: req.body.recenzent === 'on'
        };
        user.create(obj, function (id) {
            res.render('users/add_user', {message: "Dodano uzytkownika o id: " + id});
        });
      }
    });

    router.get('/list_users', auth.isAdmin, function (req, res) {
        user.selectAll(function (all) {
            res.render('users/list_users', {users: all});
        });
    });

    router.get('/edit_user/:id', auth.isAdmin, function (req, res) {
        
        var id=req.params.id;
        user.selectUser(id, function (err, user) {
            
            res.render('users/edit_user', {edited_user: user});
        });
    });
    
    
    router.get('/delete_user/:id', auth.isAdmin, function (req, res) {
        
        var id=req.params.id;
        user.deleteUser(id, function () {
            
            res.redirect('/list_users');
        });
    });

    router.post('/edit_user/:id', auth.isAdmin, function (req, res) {
        
        
        var obj = {
            email: req.body.email,
            imie: req.body.imie,
            nazwisko: req.body.nazwisko,
            adres: req.body.adres,
            firma: req.body.firma,
            nr_tel: req.body.phone,
            aktywny: req.body.optionsStatus === 'aktywny',
            adm: req.body.admin === 'on',
            oplaty: req.body.optionsOplata === 'dokonano',
            komentarz_admin: req.body.komentarz,
            recenzent: req.body.recenzent === 'on'
        };

        var id=req.params.id;
        user.updateUser(id, obj, function () {
            res.redirect('/list_users');
        });
    });


    return router;
};